#!/usr/bin/python3

#everfiles GPL3
#Copyright (C) 2019 David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.



#Uses cheats from mupen64plus and creates cheat files readable by the everdrive n64
#Use this on games you OWN, Thanks!


import os
import subprocess
import shutil
import string

mainDir = os.path.dirname(os.path.realpath(__file__))

n64RomDir = mainDir + "/n64/"
outputDir = mainDir + "/out/"
romsWeOWN = {}



############################################################################################
######################################Load roms we Own######################################
############################################################################################

#Read ./n64/* and load {'rom name (u)': '/full/path/to/rom name (u) [!].n64'}
for rom in os.listdir(n64RomDir):
  if rom.endswith('64'):
    keyName = rom.split('.')[0:-1]
    keyName = ".".join(keyName)
    #cut off [!]
    if keyName.endswith('[!]'):
      keyName = keyName[:-4]
    filepath = mainDir + "/n64/" + rom
    romsWeOWN[keyName] = filepath
    
#{name,"codes"}
MASTERCODES = {}
############################################################################################
#########################Load masterCodes needed for loading cheats#########################
############################################################################################
lines = open(mainDir + "/masterCodesUS.txt").readlines()
lines = lines + open(mainDir + "/masterCodesPAL.txt").readlines()
name = ""
codes = ""
for line in lines:
  if name == "":
    name = line
  elif line.strip() == "":
    MASTERCODES[name.strip()] = codes
    name = ""
    codes = ""
    continue
  else:
    codes = codes + line
  if line.startswith('http:'):
    break
  


############################################################################################
##############################Use Mupen64Plus to pull CRC code##############################
############################################################################################


def getCRC(romPath):
  command = f'mupen64plus --cheats list "{romPath}" | grep CRC | cut -d: -f3'
  output = subprocess.run(command, shell=True, stdout=subprocess.PIPE, universal_newlines=True).stdout
  return output.strip().replace(' ', '-')


CRCcodes = {} #{CRC: [PATH,NAME]}
#if you limit the list you limit the sample size[-60:-50]
print("Loading CRC data...")
for rom in list(romsWeOWN.keys()):
  path = romsWeOWN[rom].strip()
  CRC = getCRC(path)
  CRCcodes[CRC] = [path, rom]
  print(f"{CRC}: {CRCcodes[CRC][-1]}")



############################################################################################
#########################################Setup dirs#########################################
############################################################################################
print("Building " + outputDir)
#clean up old dirs
if os.path.exists(outputDir):
  shutil.rmtree(outputDir)
#build new dirs 
os.makedirs(outputDir)
for letter in string.ascii_uppercase + "01":
  subFolder = f"{outputDir}{letter}/"
  os.makedirs(subFolder)
  print(letter)
  for CRC in CRCcodes:
    name = CRCcodes[CRC][-1]
    path = CRCcodes[CRC][0]
    if name.upper().startswith(letter):
      print(name)
      subsubFolder =  subFolder + name + "/"
      os.makedirs(subsubFolder)
      shutil.copy2(path, subsubFolder)
      






############################################################################################
#####################################Write Master codes#####################################
############################################################################################
for rom in CRCcodes:
  name = CRCcodes[rom][1]
  path = f"{outputDir}{name[0].upper()}/{name}/Master_{name.replace(' ', '_')}.txt"
  if name in MASTERCODES:
    #print("FOUND: " + name)
    codes = MASTERCODES[name]
    with open(path, 'w') as fh:
      fh.write(codes)
  else:
    #print("Missing: " + name)
    #Try to find a close match
    namePart = name.replace("-", "(").split("(")[0].strip()
    #if name is too short, cut it less
    if len(namePart) < 3:
      namePart = name.replace("-", "(").split("(")[0].strip()
    for fullName in MASTERCODES:
      if namePart in fullName:
        #print("Found alike: " + name)
        codes = MASTERCODES[fullName]
        with open(path, 'w') as fh:
          fh.write(codes)
    
#print(MASTERCODES)
#print (CRCcodes)

############################################################################################
#########################################Write Cheat########################################
############################################################################################

#takes path to cheats and {'codename': [ffffffff ffff', 'ffffffff ffff']}
def createCheats(cheatDir, codes):
  for cheat in codes:
    fileName = cheatDir + cheat + ".txt"
    txt = ""
    for line in codes[cheat]:
      txt = txt + line + "\n"
    fh = open(fileName, 'w')
    fh.write(txt)
    fh.close()




############################################################################################
#########################################Parse cheats#######################################
############################################################################################


mupenCheatsFile = mainDir + "/cheats_raw.txt"
mupenCheats = open(mupenCheatsFile).readlines()


index = 0
while index < len(mupenCheats):
  line = mupenCheats[index]
  if line.startswith('//'):
    index = index + 1
    continue 
  if line.startswith('crc'):
    CRC = line[4:].split("-C")[0] # crc AAAAAAAA-AAAAAAAA-C:45 -> AAAAAAAA-AAAAAAAA
    if CRC in CRCcodes.keys():
      name = CRCcodes[CRC][-1]
      print(f'Adding cheats for: "{name}": {CRC}')
      #convert codes
      codeStart = False
      plusIndex = 1
      codes = {}
      codeName = ""
      note = ""
      while True:
        newLine = mupenCheats[index + plusIndex].strip()
        if newLine.startswith('gn'):
          plusIndex = plusIndex + 1
          continue
        #Handle CN
        if newLine.startswith('cn'):
          codeName = newLine[3:].replace("\\", '_').strip()
          codeName = codeName.replace(',', '_')
          codeName = codeName.replace('/', '_')
          plusIndex = plusIndex + 1
          note = ""
          continue
        if newLine.strip().startswith('cd'):
          note = newLine.strip()[3:]
          plusIndex = plusIndex + 1
          continue
        if newLine.strip() != "":
          if '?' in newLine:
            newLine = newLine.split("????")[0] + newLine.split("????")[1].split(':')[0].strip()
          if codeName not in codes.keys():
            codes[codeName] = [newLine.strip() + " " + note]
          else:
            codes[codeName].append(newLine.strip())
        if newLine.strip() == "":
          #index = index + plusIndex - 3
          cheatDir = f"{outputDir}{name[0].upper()}/{name}/cheats/"
          try:
            os.makedirs(cheatDir)
          except Exception:
            pass
          createCheats(cheatDir, codes)
          #print (str(codes))
          break
        plusIndex = plusIndex + 1

  #gn Baku Bomberman (J)
  
  index = index + 1

  
