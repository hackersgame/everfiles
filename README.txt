everfiles GPL3
Copyright (C) 2019 David Hamner

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.



Uses cheats from mupen64plus and creates cheat files readable by the everdrive n64
Use this on games you OWN, Thanks!


Setup:
  Copy roms into ./n64
  Run chmod 755 ./everfiles.py
  Run ./everfiles.py 
  Copy ./out/* onto FAT32 formatted SD card.

Usage:
  When cheating with the everdrive
  Select the rom and pick the option "Select only"
  If you see a MASTER_rom_name.txt in your rom folder, you MUST load that fist. (This will enable other cheats) 
  Append other wanted cheats
  Press the start button
